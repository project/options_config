(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.optionKey = {
    attach: function attach(context, settings) {
      var self = this;
      var $context = $(context);
      var timeout = null;
      var xhr = null;

      function clickEditHandler(e) {
        var data = e.data;
        data.$wrapper.removeClass('visually-hidden');
        data.$target.trigger('focus');
        data.$suffix.hide();
        data.$source.off('.optionKey');
      }

      function optionKeyHandler(e) {
        var data = e.data;
        var options = data.options;
        var baseValue = $(e.target).val();

        var rx = new RegExp(options.replace_pattern, 'g');
        var expected = baseValue.replace(rx, options.replace).substr(0, options.maxlength);

        if (xhr && xhr.readystate !== 4) {
          xhr.abort();
          xhr = null;
        }

        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        if (baseValue !== expected) {
          timeout = setTimeout(function () {
            xhr = self.transliterate(baseValue, options).done(function (optionKey) {
              self.showOptionKey(optionKey.substr(0, options.maxlength), data);
            });
          }, 300);
        } else {
          self.showOptionKey(expected, data);
        }
      }

      Object.keys(settings.optionKey).forEach(function (sourceId) {
        var optionKey = '';
        var options = settings.optionKey[sourceId];

        var $source = $context.find(sourceId).addClass('option-key-source').once('option-key');
        var $target = $context.find(options.target).addClass('option-key-target');
        var $suffix = $context.find(options.suffix);
        var $wrapper = $target.closest('.js-form-item');

        if (!$source.length || !$target.length || !$suffix.length || !$wrapper.length) {
          return;
        }

        if ($target.hasClass('error')) {
          return;
        }

        options.maxlength = $target.attr('maxlength');

        $wrapper.addClass('visually-hidden');

        if ($target.is(':disabled') || $target.val() !== '') {
          optionKey = $target.val();
        } else if ($source.val() !== '') {
          optionKey = self.transliterate($source.val(), options);
        }

        var $preview = $('<span class="option-key-value">' + options.field_prefix + Drupal.checkPlain(optionKey) + options.field_suffix + '</span>');
        $suffix.empty();
        if (options.label) {
          $suffix.append('<span class="option-key-label">' + options.label + ': </span>');
        }
        $suffix.append($preview);

        if ($target.is(':disabled')) {
          return;
        }

        var eventData = {
          $source: $source,
          $target: $target,
          $suffix: $suffix,
          $wrapper: $wrapper,
          $preview: $preview,
          options: options
        };

        var $link = $('<span class="admin-link"><button type="button" class="link">' + Drupal.t('Edit') + '</button></span>').on('click', eventData, clickEditHandler);
        $suffix.append($link);

        if ($target.val() === '') {
          $source.on('formUpdated.optionKey', eventData, optionKeyHandler).trigger('formUpdated.optionKey');
        }

        $target.on('invalid', eventData, clickEditHandler);
      });
    },
    showOptionKey: function showOptionKey(optionKey, data) {
      var settings = data.options;

      if (optionKey !== '') {
        if (optionKey !== settings.replace) {
          data.$target.val(optionKey);
          data.$preview.html(settings.field_prefix + Drupal.checkPlain(optionKey) + settings.field_suffix);
        }
        data.$suffix.show();
      } else {
        data.$suffix.hide();
        data.$target.val(optionKey);
        data.$preview.empty();
      }
    },
    transliterate: function transliterate(source, settings) {
      return $.get(Drupal.url('option_key/transliterate'), {
        text: source,
        langcode: drupalSettings.langcode,
        replace_pattern: settings.replace_pattern,
        replace_token: settings.replace_token,
        replace: settings.replace,
        lowercase: false
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
