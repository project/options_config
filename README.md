CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Custom forms
* Options Providers

INTRODUCTION
------------
Options Config is a module that provides a configurable taxonomy system.
You can create Options List configuration entities that can be used as options lists in fields and custom forms.

When you use the Options List field type on your node/entity, you can select the saved options lists to be displayed in
forms.

When you create an option list you can choose the Option provider plugin used for preparing the options.
Out of the box two plugins are included

* Default: Lets the user create list of ket/value pairs
* Taxonomy: Lets the user select a vocabulary for exposing it as option list
* Years range: Lets the user select a year from a configured list

Custom plugins can be easily created and included in custom modules to provide to the user options dynamically
generated or retrieved from external sources.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:

  - Administer options list (Configurable options)

    Allows users to create and edit Options Lists under Structure » Options list.

* Create options lists under Structure » Options list.

* Add a field to any entity type and select the Option List field type and select an Option List entity.

CUSTOM FORMS
------------

Option lists can be added in custom forms using any form element that allows '#options'.
```php
$form['KEY'] = [
  'type' => 'select',
  '#options' => OptionsList::load('options_list_machine_name')->getOptions()
];
```

OPTIONS PROVIDERS
-----------------

Options Providers are the plugins that prepare options to be served in forms.
Custom Options Providers can be included in custom modules under the `Plugin/OptionsProvider` folder.
See providers examples provided by this module.
