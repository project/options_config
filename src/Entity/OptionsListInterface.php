<?php

namespace Drupal\options_config\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
/**
 * Provides an interface for defining Options list entities.
 */
interface OptionsListInterface extends ConfigEntityInterface {

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\options_config\Plugin\OptionsProviderInterface
   *   The plugin instance for this options list.
   */
  public function getPlugin();

  /**
   * Returns the plugin ID.
   *
   * @return string
   *   The plugin ID for this OptionsList.
   */
  public function getPluginId(): string;

  /**
   * Get the array of prepared options in the format [key => label].
   *
   * @return array
   *  The prepared options array.
   */
  public function getOptions(): array;
  
  /**
   * Get an option label by key.
   *
   * @param string $key
   *   The option key.
   *
   * @return string
   *  The option label.
   */
  public function getOptionLabel(string $key): string;
  
}
