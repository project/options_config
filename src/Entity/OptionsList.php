<?php

namespace Drupal\options_config\Entity;

use Drupal\Component\Plugin\LazyPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\options_config\OptionsListPluginCollection;

/**
 * Defines the Options list entity.
 *
 * @ConfigEntityType(
 *   id = "options_list",
 *   label = @Translation("Options list"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\options_config\OptionsListListBuilder",
 *     "form" = {
 *       "add" = "Drupal\options_config\Form\OptionsListForm",
 *       "edit" = "Drupal\options_config\Form\OptionsListForm",
 *       "delete" = "Drupal\options_config\Form\OptionsListDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\options_config\OptionsListHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "options_list",
 *   admin_permission = "administer options list",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "plugin" = "plugin",
 *     "settings" = "settings",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "plugin",
 *     "settings",
 *     "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/options_list/{options_list}",
 *     "add-form" = "/admin/structure/options_list/add",
 *     "edit-form" = "/admin/structure/options_list/{options_list}/edit",
 *     "delete-form" = "/admin/structure/options_list/{options_list}/delete",
 *     "collection" = "/admin/structure/options_list"
 *   }
 * )
 */
class OptionsList extends ConfigEntityBase implements OptionsListInterface, EntityWithPluginCollectionInterface {

  /**
   * The Options list ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Options list label.
   *
   * @var string
   */
  protected $label;

  /**
   * @var
   */
  protected $plugin;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The plugin collection that holds the options provider plugin for this entity.
   *
   * @var \Drupal\options_config\OptionsListPluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'settings' => $this->getPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId():string {
    return $this->plugin ?? 'default';
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    return $this->getPlugin()->getOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionLabel(string $key): string {
    return array_key_exists($key, $this->getOptions()) ? $this->getOptions()[$key] : '';
  }

  /**
   * Encapsulates the creation of the block's LazyPluginCollection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The options list's plugin collection.
   */
  protected function getPluginCollection(): LazyPluginCollection {
    if (!$this->pluginCollection) {
      $this->pluginCollection = new OptionsListPluginCollection(\Drupal::service('plugin.manager.options_provider'), $this->getPluginId(), $this->get('settings'), $this->id());
    }
    return $this->pluginCollection;
  }

}
