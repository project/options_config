<?php

namespace Drupal\options_config;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\options_config\Entity\OptionsListInterface;

/**
 * Class OptionsconfigHelper.
 */
class OptionsConfigHelper {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new OptionsconfigHelper object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Get available options list entities.
   *
   * @return \Drupal\options_config\Entity\OptionsListInterface[]
   *   An array of options list entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAvailablelists(): array {
    return $this->entityTypeManager->getStorage('options_list')->loadMultiple();
  }

  /**
   * Get prepared options from a specific options list ID.
   *
   * @param string $id
   *  The OptionsList entity ID.
   *
   * @return array
   *   The prepared options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOptionsByEntityId(string $id): array {
    /** @var OptionsListInterface $options_list */
    $options_list = $this->entityTypeManager->getStorage('options_list')->load($id);
    return $options_list->getOptions();
  }

}
