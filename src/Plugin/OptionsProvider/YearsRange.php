<?php

namespace Drupal\options_config\Plugin\OptionsProvider;

use Drupal\Core\Form\SubformStateInterface;
use Drupal\options_config\Plugin\OptionsProviderBase;

/**
 * Plugin implementation of the 'Years of birth' options provider.
 *
 * @OptionsProvider(
 *   id = "years_range",
 *   label = @Translation("Years range"),
 * )
 */
class YearsRange extends OptionsProviderBase {

  public function defaultConfiguration() {
    return [
      'first' => 1900,
      'last' => (int) date('Y'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderForm($form, SubformStateInterface $form_state): array {

    $base_options = $this->defaultConfiguration();

    $form['first'] = [
      '#type' => 'select',
      '#title' => $this->t('Starting Year'),
      '#options' => $this->getYearsOptions($base_options['first'], $base_options['last'], TRUE),
      '#default_value' => (int) $this->configuration['first'],
    ];

    $form['last'] = [
      '#type' => 'select',
      '#title' => $this->t('Last Year'),
      '#options' => $this->getYearsOptions($base_options['first'], $base_options['last']),
      '#default_value' => (int) $this->configuration['last'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareOptions(): array {
    return $this->getYearsOptions($this->configuration['first'], $this->configuration['last']);
  }

  /**
   * {@inheritdoc}
   */
  public function getYearsOptions(int $first, int $last, $reverse = FALSE): array {
    $years = array_combine(range($first, $last), range($first, $last));
    return $reverse ? array_reverse($years, TRUE) : $years;
  }

}
