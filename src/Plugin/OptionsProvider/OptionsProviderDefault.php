<?php

namespace Drupal\options_config\Plugin\OptionsProvider;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\options_config\Plugin\OptionsProviderBase;

/**
 * Plugin implementation of the 'default' options provider.
 *
 * @OptionsProvider(
 *   id = "default",
 *   label = @Translation("Default"),
 * )
 */
class OptionsProviderDefault extends OptionsProviderBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'options' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderForm($form, SubformStateInterface $form_state): array {

    if (!$form_state->get('options')) {
      $form_state->set('options', $this->configuration['options']);
    }

    $group_class = 'group-order-weight';
    $form['options'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Option Label'),
        $this->t('Option key'),
        $this->t('Weight'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('No options.'),
      '#tableselect' => FALSE,
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => $group_class,
        ]
      ],
      '#attributes' => [
        'class' => ['options-config-table']
      ],
    ];

    $options = $this->getFormOptions($form_state->getCompleteFormState());
    $new_weight = 0;
    $new_delta = 0;

    // Build rows.
    foreach ($options as $delta => $value) {
      $form['options'][$delta]['#attributes']['class'][] = 'draggable';
      $form['options'][$delta]['#weight'] = $value['weight'];

      // Label col.
      $form['options'][$delta]['label'] = [
        '#type' => 'textfield',
        '#default_value' => $value['label'],
        '#max_length' => 64,
        '#size' => 64,
      ];

      $form['options'][$delta]['key'] = [
        '#type' => 'textfield',
        '#default_value' => $value['key'],
        '#disabled' => TRUE,
      ];

      // Weight col.
      $form['options'][$delta]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $value['label']]),
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'],
        '#attributes' => ['class' => [$group_class]],
      ];

      $form['options'][$delta]['remove'] = [
        '#type' => 'button',
        '#name' => 'remove_option__' . $delta,
        '#value' => $this->t('Remove'),
        '#op' => 'remove',
        '#delta' => $delta,
        '#limit_validation_errors' => [
          ['new_option']
        ],
        '#ajax' => [
          'wrapper' => 'options-list-table',
          'callback' => [$this, 'ajaxRefreshForm']
        ],
      ];

      $new_weight = $value['weight']++;
      $new_delta = $delta + 1;
    }

    $form['options'][$new_delta]['#weight'] = $new_weight;

    // Label col.
    $form['options'][$new_delta]['label'] = [
      '#type' => 'textfield',
      '#default_value' => '',
      '#max_length' => 64,
      '#size' => 64,
    ];

    $form['options'][$new_delta]['key'] = [
      '#type' => 'option_key',
      '#default_value' => '',
      '#required' => FALSE,
      '#option_key' => [
        'source' => ['settings', 'options', $new_delta, 'label'],
        'exists' => [$this, 'optionExists'],
        'standalone' => TRUE,
        'replace_pattern' => '[^\w\-\s]+'
      ],
    ];

    // Weight col.
    $form['options'][$new_delta]['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for @title', ['@title' => 'the new option']),
      '#title_display' => 'invisible',
      '#default_value' => $new_weight,
      '#attributes' => ['class' => [$group_class]],
    ];

    $form['options'][$new_delta]['add'] = [
      '#type' => 'button',
      '#name' => 'add_option',
      '#op' => 'add',
      '#value' => $this->t('Add'),
      '#limit_validation_errors' => [
        ['settings', 'options']
      ],
      '#ajax' => [
        'wrapper' => 'options-list-table',
        'callback' => [$this, 'ajaxRefreshForm']
      ],
    ];
    return $form + [
      '#prefix' => '<div id="options-list-table">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * Get the updated options.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   The options available in the plugin configuration.
   */
  public function getFormOptions(FormStateInterface $form_state): array {
    $trigger = $form_state->getTriggeringElement();
    $op = isset($trigger['#op']) ? $trigger['#op'] : NULL;

    if ($op) {
      switch ($op) {
        case 'add':
          $this->setOptions($form_state);
          break;
        case 'remove':
          $this->removeOption($trigger['#delta'], $form_state);
          break;
      }
    }
    return $this->configuration['options'];
  }


  /**
   * Set the configuration options from form values.
   *
   * @param array $values
   *   The array of options form values.
   */
  public function setOptions(FormStateInterface $form_state) {
    $values = $form_state->getValue(['settings', 'options']);

    $options = array_filter($values, function($option) {
      return !empty($option['key']);
    });

    foreach ($options as $i => &$option) {
      $option = array_filter($option, function($value, $key) {
        $valid_keys = ['label', 'key', 'weight'];
        return in_array($key, $valid_keys);
      }, ARRAY_FILTER_USE_BOTH);
    }

    $this->configuration['options'] = $options;
  }

  /**
   * Remove an option from the configuration.
   *
   * @param int $delta
   *   The delta of the option being removed.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete FormState object.
   */
  public function removeOption(int $delta, FormStateInterface $form_state) {
    unset($this->configuration['options'][$delta]);
    $this->configuration['options'] = array_values($this->configuration['options']);

    // Clear empty option input.
    $input = $form_state->getUserInput();
    unset($input['settings']['options'][$delta]);
    $input['settings']['options'] = array_values($input['settings']['options']);
    $form_state->setUserInput($input);
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderSubmit($form, SubformStateInterface $form_state) {
    $this->setOptions($form_state->getCompleteFormState());
  }

  /**
   * Check if an option exists already in the list.
   *
   * @param string $value
   *   The option key that needs to be checked.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *  The form state.
   *
   * @return bool
   *   Whether the key exists already in the options list.
   */
  public function optionExists(string $value, array $element, FormStateInterface $form_state): bool {
    $options_list = $this->configuration['options'];
    foreach ($options_list as $option) {
      if ($option['key'] == $value) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareOptions(): array {
    $prepared_options = array_combine(
      array_map( function($option) {
        return $option['key'];
      }, $this->configuration['options']),
      array_map( function($option) {
        return $option['label'];
      }, $this->configuration['options'])
    );
    return $prepared_options;
  }

  /**
   * Ajax handler for refreshing an entire form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The options table.
   */
  public function ajaxRefreshForm(array $form, FormStateInterface $form_state) {
    return $form['settings'];
  }

}
