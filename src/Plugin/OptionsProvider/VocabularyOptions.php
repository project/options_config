<?php

namespace Drupal\options_config\Plugin\OptionsProvider;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\options_config\Plugin\OptionsProviderBase;
use Drupal\taxonomy\VocabularyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'vocabulary' options provider.
 *
 * @OptionsProvider(
 *   id = "vocabulary",
 *   label = @Translation("Vocabulary"),
 * )
 */
class VocabularyOptions extends OptionsProviderBase {
  
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *
   */
  protected $entityTypeManager;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'vocabulary' => '',
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function optionsProviderForm($form, SubformStateInterface $form_state): array {
    /** @var \Drupal\taxonomy\VocabularyInterface[] $vocabularies */
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocabulary_options = array_combine(
      array_map(function(VocabularyInterface $v) {
        return $v->id();
      }, $vocabularies),
      array_map(function(VocabularyInterface  $v) {
        return $v->label();
      }, $vocabularies)
    );
    
    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => $vocabulary_options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['vocabulary'],
    ];

    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function prepareOptions(): array {
    $options = [];
  
    /** @var \stdClass[] $terms */
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($this->configuration['vocabulary']);
  
    if (empty($terms)) {
      return $options;
    }
  
    $options = array_combine(
      array_map( function(\stdClass$term) {
        return $this->getMachineName($term->name);
      }, $terms),
      array_map( function(\stdClass $term) {
        return $term->name;
      }, $terms)
    );
  
    return $options;
  }
  
  /**
   * Get machine name based on name.
   *
   * @param string $name
   *   Name.
   *
   * @return string
   *   Machine name.
   */
  public function getMachineName(string $name): string {
    $machine_name = strtolower(trim($name));
    $machine_name = preg_replace('/[^a-z0-9]+/', '_', $machine_name);
    $machine_name = preg_replace('/-+/', '_', $machine_name);
    $machine_name = preg_replace('/_+/', '_', $machine_name);
    
    return $machine_name;
  }
  
  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    /** @var VocabularyInterface $vocabulary */
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($this->configuration['vocabulary']);
    
    return [
      'module' => ['taxonomy'],
      'config' => [$vocabulary->getConfigDependencyName()],
    ];
  }
  
}
