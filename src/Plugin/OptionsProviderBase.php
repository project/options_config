<?php

namespace Drupal\options_config\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\options_config\Entity\OptionsListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Options provider plugins.
 */
abstract class OptionsProviderBase extends PluginBase implements OptionsProviderInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use PluginWithFormsTrait;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->getPluginDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep(
      $this->baseConfigurationDefaults(),
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * Returns generic default configuration for block plugins.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  protected function baseConfigurationDefaults() {
    return [
      'other_option' => FALSE,
      'other_option_label' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigurationValue($key, $value) {
    $this->configuration[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }
  
  /**
   * Reset configuration on plugin selection.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function initConfig(FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $trigger_op = isset($trigger['#op']) ? $trigger['#op'] : '';
    if ($trigger_op == 'plugin_select') {
      $this->configuration = $this->defaultConfiguration();
    }
  }
  
  /**
   * {@inheritdoc}
   *
   * Creates a generic configuration form for all options provider plugins. Individual
   * plugins can add elements to this form by overriding
   * OptionsProviderBase::optionsProviderForm(). Most block plugins should not override this
   * method unless they need to alter the generic form elements.
   *
   * @see \Drupal\options_config\Plugin\OptionsProviderBase
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->initConfig($form_state);
    $form += $this->optionsProviderForm($form, $form_state);
  
    $form['other_option'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include "Other" option'),
      '#default_value' => $this->configuration['other_option'],
    ];
  
    $form['other_option_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Other" option label'),
      '#default_value' => $this->configuration['other_option_label'],
      '#states' => [
        'visible' => [
          'input[name="settings[other_option]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="settings[other_option]' => ['checked' => TRUE],
        ],
      ],
    ];
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderForm($form, SubformStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * Most options provider plugins should not override this method. To add validation
   * for a specific options provider, override OptionsProviderBase::optionsProviderValidate().
   *
   * @see \Drupal\options_config\Plugin\OptionsProviderBase::OptionsProviderValidate()
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->optionsProviderValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderValidate($form, SubformStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   *
   * Most options provider plugins should not override this method. To add submission
   * handling for a specific options provider, override OptionsProviderBase::optionsProviderSubmit().
   *
   * @see \Drupal\options_config\Plugin\OptionsProviderBase::OptionsProviderSubmit()
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getCompleteFormState()->getValue(['settings']);
    foreach ($this->baseConfigurationDefaults() as $key => $value) {
      if (isset($values[$key])) {
        $this->configuration[$key] = $values[$key];
      }
      
      $this->optionsProviderSubmit($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function optionsProviderSubmit($form, SubformStateInterface $form_state) {
    $values = $form_state->getCompleteFormState()->getValue(['settings']);
    foreach ($this->defaultConfiguration() as $required => $default) {
      if (array_key_exists($required, $values)) {
        $this->configuration[$required] = $values[$required];
      }
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    $options = $this->prepareOptions();
    if ($this->configuration['other_option']) {
      $options['other'] = $this->configuration['other_option_label'];
    }
    return $options;
  }

}
