<?php

namespace Drupal\options_config\Plugin\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines the configurable 'Options List' field type.
 */
class OptionsListItemList extends FieldItemList {

  /**
   * Return an array of selected options.
   *
   * @return array
   *   The options stored against the field.
   */
  public function referencedOptionKeys(): array {
    if (empty($this->list)) {
      return [];
    }

    foreach ($this->list as $delta => $item) {
      if ((bool) $item->value) {
        $referenced_options[$delta] = $item->value;
      }
    }

    return $referenced_options;
  }
}
