<?php

namespace Drupal\options_config\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;

/**
 * Defines the configurable 'Options List' field type.
 *
 * @FieldType(
 *   id = "options_list",
 *   label = @Translation("Options list"),
 *   description = @Translation("An list field containing configurable options."),
 *   default_widget = "options_list_select",
 *   default_formatter = "options_list_default",
 *   list_class = "\Drupal\options_config\Plugin\Field\OptionsListItemList",
 * )
 */
class OptionsListItem extends StringItemBase implements OptionsProviderInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'options_list_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // This is called very early by the user entity roles field. Prevent
    // early t() calls by using the TranslatableMarkup.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text value'))
      ->setSetting('options_list_id', $field_definition->getSetting('options_list_id'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];
    $available = [];

    /** @var \Drupal\options_config\OptionsConfigHelper $options_list_helper */
    $options_list_helper = \Drupal::service('options_config.helper');
    $options_lists = $options_list_helper->getAvailablelists();

    foreach ($options_lists as $options_list) {
      $available[$options_list->id()] = $options_list->label();
    }

    $element['options_list_id'] = [
      '#type' => 'select',
      '#title' => t('Options List'),
      '#default_value' => $this->getSetting('options_list_id'),
      '#options' => $available,
      '#required' => TRUE,
      '#description' => t('The configurable options list for this field.'),
      '#disabled' => $has_data,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    // Flatten options firstly, because Possible Options may contain group
    // arrays.
    $flatten_options = OptGroup::flattenOptions($this->getPossibleOptions($account));
    return array_keys($flatten_options);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return $this->getSettableOptions($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    // Flatten options firstly, because Settable Options may contain group
    // arrays.
    $flatten_options = OptGroup::flattenOptions($this->getSettableOptions($account));
    return array_keys($flatten_options);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL) {
    $allowed_options = options_config_allowed_values($this->getFieldDefinition()->getFieldStorageDefinition(), $this->getEntity());
    return $allowed_options;
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateDependencies(FieldDefinitionInterface $field_definition) {
    $dependencies = parent::calculateDependencies($field_definition);
    $manager = \Drupal::entityTypeManager();
    $target_entity_type = 'options_list';
    $options_list_id = $field_definition->getFieldStorageDefinition()->getSetting('options_list_id');

    if ($options_list_id && $storage = $manager->getStorage($target_entity_type)) {
      /** @var \Drupal\options_config\Entity\OptionsListInterface $options_list */
      $options_list = $storage->load($options_list_id);
      $dependencies[$options_list->getConfigDependencyKey()][] = $options_list->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public static function calculateStorageDependencies(FieldStorageDefinitionInterface $field_storage_definition) {
    $dependencies = parent::calculateStorageDependencies($field_storage_definition);
    $manager = \Drupal::entityTypeManager();
    $target_entity_type = 'options_list';
    $options_list_id = $field_storage_definition->getSetting('options_list_id');

    if ($options_list_id && $storage = $manager->getStorage($target_entity_type)) {
      /** @var \Drupal\options_config\Entity\OptionsListInterface $options_list */
      $options_list = $storage->load($options_list_id);
      $dependencies[$options_list->getConfigDependencyKey()][] = $options_list->getConfigDependencyName();
    }

    return $dependencies;
  }

}
