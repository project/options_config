<?php

namespace Drupal\options_config\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "options_list_select",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "options_list"
 *   },
 *   multiple_values = TRUE
 * )
 */
class OptionsListSelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'empty_option_label' => t('- None -'),
        'select_value_label' => t('- Select a value -'),
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['empty_option_label'] = [
      '#type' => 'textfield',
      '#title' => t('"Empty Option" label'),
      '#default_value' => $this->getSetting('empty_option_label'),
      '#required' => TRUE,
    ];

    $element['select_value_label'] = [
      '#type' => 'textfield',
      '#title' => t('"Select a value" label'),
      '#default_value' => $this->getSetting('select_value_label'),
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('"Empty option" label: @label', ['@label' => $this->getSetting('empty_option_label')]);
    $summary[] = t('"Select a value" label: @label', ['@label' => $this->getSetting('select_value_label')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if ($this->multiple) {
      // Multiple select: add a 'none' option for non-required fields.
      if (!$this->required) {
        return $this->getSetting('empty_option_label');
      }
    }
    else {
      // Single select: add a 'none' option for non-required fields,
      // and a 'select a value' option for required fields that do not come
      // with a value selected.
      if (!$this->required) {
        return $this->getSetting('empty_option_label');
      }
      if (!$this->has_value) {
        return $this->getSetting('select_value_label');
      }
    }
  }

}
