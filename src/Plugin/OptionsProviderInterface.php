<?php

namespace Drupal\options_config\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines an interface for Options provider plugins.
 */
interface OptionsProviderInterface extends ConfigurableInterface, DependentPluginInterface, PluginInspectionInterface, PluginFormInterface {

  /**
   * Return the label of the option provider plugin.
   *
   * @return string
   */
  public function label(): string ;

  /**
   * Get the options array provided by the plugin including base configuration.
   *
   * @return array
   *   An array of options ready to be used in form elements or fields.
   */
  public function getOptions(): array;
  

  /**
   * Get the options array provided by the plugin.
   *
   * This is the only compulsory method when creating a new plugin.
   *
   * @return array
   *   An array of options returned by the plugin.
   */
  public function prepareOptions(): array;
  
  /**
   * Build the plugin form.
   *
   * It will be appended to the OptionsList 'settings' entity form element.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return array
   *   The prepared form array.
   */
  public function optionsProviderForm($form, SubformStateInterface $form_state): array;
  
  /**
   * Validate the plugin form.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   */
  public function optionsProviderValidate($form, SubformStateInterface $form_state);
  
  /**
   * Submit the plugin form.
   *
   * @param $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   */
  public function optionsProviderSubmit($form, SubformStateInterface $form_state);
}
