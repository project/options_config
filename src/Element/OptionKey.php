<?php

namespace Drupal\options_config\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Element\Textfield;

/**
 * Provides an option key render element.
 *
 * Provides a form element to enter an option key, which is validated to ensure
 * that the key is unique and does not contain disallowed characters.
 *
 * The element may be automatically populated via JavaScript when used in
 * conjunction with a separate "source" form element (typically specifying the
 * human-readable name). As the user types text into the source element, the
 * JavaScript converts all values to lower case, replaces any remaining
 * disallowed characters with a replacement, and populates the associated
 * option key form element.
 *
 * Properties:
 * - #option_key: An associative array containing:
 *   - exists: A callable to invoke for checking whether a submitted option
 *     key value already exists. The arguments passed to the callback will be:
 *     - The submitted value.
 *     - The element array.
 *     - The form state object.
 *     In most cases, an existing API or menu argument loader function can be
 *     re-used. The callback is only invoked if the submitted value differs from
 *     the element's initial #default_value. The initial #default_value is
 *     stored in form state so AJAX forms can be reliably validated.
 *   - source: (optional) The #array_parents of the form element containing the
 *     human-readable name (i.e., as contained in the $form structure) to use as
 *     source for the option key. Defaults to array('label').
 *   - label: (optional) Text to display as label for the option key value
 *     after the human-readable name form element. Defaults to t('Option key').
 *   - replace_pattern: (optional) A regular expression (without delimiters)
 *     matching disallowed characters in the option key. Defaults to
 *     '[^a-z0-9_]+'.
 *   - replace: (optional) A character to replace disallowed characters in the
 *     option key via JavaScript. Defaults to '_' (underscore). When using a
 *     different character, 'replace_pattern' needs to be set accordingly.
 *   - error: (optional) A custom form error message string to show, if the
 *     option key contains disallowed characters.
 *   - standalone: (optional) Whether the live preview should stay in its own
 *     form element rather than in the suffix of the source element. Defaults
 *     to FALSE.
 * - #maxlength: (optional) Maximum allowed length of the option key. Defaults
 *   to 64.
 * - #disabled: (optional) Should be set to TRUE if an existing option key
 *   must not be changed after initial creation.
 *
 * Usage example:
 * @code
 * $form['id'] = array(
 *   '#type' => 'option_key',
 *   '#default_value' => $this->entity->id(),
 *   '#disabled' => !$this->entity->isNew(),
 *   '#maxlength' => 64,
 *   '#description' => $this->t('A unique name for this item. It must only contain lowercase letters, numbers, and underscores.'),
 *   '#option_key' => array(
 *     'exists' => array($this, 'exists'),
 *   ),
 * );
 * @endcode
 *
 * @see \Drupal\Core\Render\Element\Textfield
 *
 * @FormElement("option_key")
 */
class Optionkey extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#default_value' => NULL,
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 60,
      '#autocomplete_route_name' => FALSE,
      '#process' => [
        [$class, 'processOptionkey'],
        [$class, 'processAutocomplete'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateOptionkey'],
      ],
      '#pre_render' => [
        [$class, 'preRenderTextfield'],
      ],
      '#theme' => 'input__textfield',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      // This should be a string, but allow other scalars since they might be
      // valid input in programmatic form submissions.
      return is_scalar($input) ? (string) $input : '';
    }
    return NULL;
  }

  /**
   * Processes a option key form element.
   *
   * @param array $element
   *   The form element to process. See main class documentation for properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processOptionkey(&$element, FormStateInterface $form_state, &$complete_form) {
    // We need to pass the langcode to the client.
    $language = \Drupal::languageManager()->getCurrentLanguage();

    // Apply default form element properties.
    $element += [
      '#title' => t('Option key'),
      '#description' => t('A unique option key. Can contain alphanumeric characters, spaces, dashes and underscores.'),
      '#option_key' => [],
      '#field_prefix' => '',
      '#field_suffix' => '',
      '#suffix' => '',
    ];
    // A form element that only wants to set one #option_key property (usually
    // 'source' only) would leave all other properties undefined, if the defaults
    // were defined by an element plugin. Therefore, we apply the defaults here.
    $element['#option_key'] += [
      'source' => ['label'],
      'target' => '#' . $element['#id'],
      'label' => t('Option key'),
      'replace_pattern' => '[^\w\-\s]+',
      'replace' => '_',
      'standalone' => FALSE,
      'field_prefix' => $element['#field_prefix'],
      'field_suffix' => $element['#field_suffix'],
    ];

    // Store the initial value in form state. The option key needs this to
    // ensure that the exists function is not called for existing values when
    // editing them.
    $initial_values = $form_state->get('option_key.initial_values') ?: [];
    // Store the initial values in an array so we can differentiate between a
    // NULL default value and a new option key element.
    if (!array_key_exists($element['#name'], $initial_values)) {
      $initial_values[$element['#name']] = $element['#default_value'];
      $form_state->set('option_key.initial_values', $initial_values);
    }

    // By default, option keys are restricted to Latin alphanumeric characters.
    // So, default to LTR directionality.
    if (!isset($element['#attributes'])) {
      $element['#attributes'] = [];
    }
    $element['#attributes'] += ['dir' => LanguageInterface::DIRECTION_LTR];

    // The source element defaults to array('name'), but may have been overridden.
    if (empty($element['#option_key']['source'])) {
      return $element;
    }

    // Retrieve the form element containing the human-readable name from the
    // complete form in $form_state. By reference, because we may need to append
    // a #field_suffix that will hold the live preview.
    $key_exists = NULL;
    $source = NestedArray::getValue($form_state->getCompleteForm(), $element['#option_key']['source'], $key_exists);
    if (!$key_exists) {
      return $element;
    }

    $suffix_id = $source['#id'] . '-option-name-suffix';
    $element['#option_key']['suffix'] = '#' . $suffix_id;

    if ($element['#option_key']['standalone']) {
      $element['#suffix'] = $element['#suffix'] . ' <small id="' . $suffix_id . '">&nbsp;</small>';
    }
    else {
      // Append a field suffix to the source form element, which will contain
      // the live preview of the option key.
      $source += ['#field_suffix' => ''];
      $source['#field_suffix'] = $source['#field_suffix'] . ' <small id="' . $suffix_id . '">&nbsp;</small>';

      $parents = array_merge($element['#option_key']['source'], ['#field_suffix']);
      NestedArray::setValue($form_state->getCompleteForm(), $parents, $source['#field_suffix']);
    }

    $element['#attached']['library'][] = 'options_config/option_key';
    $options = [
      'replace_pattern',
      'replace_token',
      'replace',
      'maxlength',
      'target',
      'label',
      'field_prefix',
      'field_suffix',
      'suffix',
    ];

    /** @var \Drupal\Core\Access\CsrfTokenGenerator $token_generator */
    $token_generator = \Drupal::service('csrf_token');
    $element['#option_key']['replace_token'] = $token_generator->get($element['#option_key']['replace_pattern']);

    $element['#attached']['drupalSettings']['optionKey']['#' . $source['#id']] = array_intersect_key($element['#option_key'], array_flip($options));
    $element['#attached']['drupalSettings']['langcode'] = $language->getId();

    return $element;
  }

  /**
   * Form element validation handler for option_key elements.
   *
   * Note that #maxlength is validated by _form_validate() already.
   *
   * This checks that the submitted value:
   * - Does not contain the replacement character only.
   * - Does not contain disallowed characters.
   * - Is unique; i.e., does not already exist.
   * - Does not exceed the maximum length (via #maxlength).
   * - Cannot be changed after creation (via #disabled).
   */
  public static function validateOptionkey(&$element, FormStateInterface $form_state, &$complete_form) {
    // Verify that the option key not only consists of replacement tokens.
    if (preg_match('@^' . $element['#option_key']['replace'] . '+$@', $element['#value'])) {
      $form_state->setError($element, t('The option key must contain unique characters.'));
    }

    // Verify that the option key contains no disallowed characters.
    if (preg_match('@' . $element['#option_key']['replace_pattern'] . '@', $element['#value'])) {
      if (!isset($element['#option_key']['error'])) {
        // Since a hyphen is the most common alternative replacement character,
        // a corresponding validation error message is supported here.
        if ($element['#option_key']['replace'] == '-') {
          $form_state->setError($element, t('The option key must contain only lowercase letters, numbers, and hyphens.'));
        }
        // Otherwise, we assume the default (underscore).
        else {
          $form_state->setError($element, t('The option key must contain only lowercase letters, numbers, and underscores.'));
        }
      }
      else {
        $form_state->setError($element, $element['#option_key']['error']);
      }
    }

    // Verify that the option key is unique. If the value matches the initial
    // default value then it does not need to be validated as the option key
    // element assumes the form is editing the existing value.
    $initial_values = $form_state->get('option_key.initial_values') ?: [];
    if (!array_key_exists($element['#name'], $initial_values) || $initial_values[$element['#name']] !== $element['#value']) {
      $function = $element['#option_key']['exists'];
      if (call_user_func($function, $element['#value'], $element, $form_state)) {
        $form_state->setError($element, t('The option key is already in use. It must be unique.'));
      }
    }
  }

}
