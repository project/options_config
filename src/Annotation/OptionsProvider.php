<?php

namespace Drupal\options_config\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Options provider item annotation object.
 *
 * @see \Drupal\options_config\Plugin\OptionsProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class OptionsProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
