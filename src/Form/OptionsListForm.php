<?php

namespace Drupal\options_config\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\options_config\Plugin\OptionsProviderInterface;
use Drupal\options_config\Plugin\OptionsProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OptionsListForm.
 */
class OptionsListForm extends EntityForm {

  /**
   * The Option list being used by this form.
   *
   * @var \Drupal\options_config\Entity\OptionsListInterface
   */
  protected $entity;

  /**
   * The options provider plugin manager.
   *
   * @var \Drupal\options_config\Plugin\OptionsProviderManager
   */
  protected $optionsPluginManager;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * OptionsListForm constructor.
   *
   * @param \Drupal\options_config\Plugin\OptionsProviderManager $options_provider_manager
   *   OptionsProvider plugin type manager.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $plugin_form_factory
   *   PluginFormFactory service.
   *
   */
  public function __construct(
    OptionsProviderManager $options_provider_manager,
    PluginFormFactoryInterface $plugin_form_factory
  ) {
    $this->optionsPluginManager = $options_provider_manager;
    $this->pluginFormFactory = $plugin_form_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.options_provider'),
      $container->get('plugin_form.factory')
    );
  }

  /**
   * Prepare options for the plugin selector.
   *
   * @return array
   *   The available OptionsProvider plugins.
   */
  public function getPluginOptions(): array {
    $plugins = $this->optionsPluginManager->getDefinitions();
    return array_combine(
      array_map(function($plugin) {
        return $plugin['id'];
      }, $plugins),
      array_map(function($plugin) {
        return $plugin['label'];
      }, $plugins)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    /** @var \Drupal\options_config\Entity\OptionsListInterface $options_list */
    $options_list = $this->entity;
    $form_state->set('options_list', $options_list);
    $wrapper = 'options-list-form';

    $form['#tree'] = TRUE;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $options_list->label(),
      '#description' => $this->t('Label for the Options list.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $options_list->id(),
      '#machine_name' => [
        'exists' => '\Drupal\options_config\Entity\OptionsList::load',
      ],
      '#disabled' => !$options_list->isNew(),
    ];

    $plugin_options = $this->getPluginOptions();

    if (count($plugin_options) > 1) {
      $form['plugin'] = [
        '#type' => 'select',
        '#title' => $this->t('Options provider'),
        '#options' => $this->getPluginOptions(),
        '#default_value' => $options_list->getPluginId(),
        '#required' => TRUE,
        '#op' => 'plugin_select',
        '#ajax' => [
          'wrapper' => $wrapper,
          'callback' => [$this, 'ajaxRefreshForm']
        ],
      ];
    }
    else {
      $form['plugin'] = [
        '#type' => 'hidden',
        '#value' => 'default',
      ];
    }

    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings'] = $this->getPluginForm($options_list->getPlugin())->buildConfigurationForm($form['settings'], $subform_state);

    return $form + [
      '#prefix' => '<div id="options-list-form">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->getPluginForm($this->entity->getPlugin())->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $entity = $this->entity;
    $sub_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    // Call the plugin submit handler.
    $options_list_provider = $entity->getPlugin();
    $this->getPluginForm($options_list_provider)->submitConfigurationForm($form, $sub_form_state);

    // Save the settings of the plugin.
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $properties = $this->entity->getEntityType()->getKeys();
    if ($this->entity instanceof EntityWithPluginCollectionInterface) {
      // Do not manually update values represented by plugin collections.
      $values = array_diff_key($values, $this->entity->getPluginCollections());
    }

    foreach ($values as $key => $value) {
      if (array_key_exists($key, $properties)) {
        $entity->set($key, $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $options_list = $this->entity;
    $status = $options_list->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Options list.', [
          '%label' => $options_list->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Options list.', [
          '%label' => $options_list->label(),
        ]));
    }
    $form_state->setRedirectUrl($options_list->toUrl('collection'));
  }

  /**
   * Ajax handler for refreshing an entire form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The options table.
   */
  public static function ajaxRefreshForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Retrieves the plugin form for a given block and operation.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $block
   *   The block plugin.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The plugin form for the block.
   */
  protected function getPluginForm(OptionsProviderInterface $options_provider) {
    if ($options_provider instanceof PluginWithFormsInterface) {
      return $this->pluginFormFactory->createInstance($options_provider, 'configure');
    }
    return $options_provider;
  }

}
